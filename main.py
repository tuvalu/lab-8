import psycopg2

conn = psycopg2.connect(
    "dbname=lab host=localhost password=admin user=admin")

price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"

buy_add_inventory = f"UPDATE Inventory SET amount = amount + %(amount)s WHERE username = %(username)s AND product = %(product)s"
get_user_stock = f"SELECT SUM(amount) FROM Inventory WHERE username = %(username)s"


def buy_product(username, product, amount):
    if amount < 0:
        raise Exception("Cannot buy negative product amount")

    with conn:
        with conn.cursor() as cur:
            obj = {"product": product, "username": username, "amount": amount}

            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad balance")

            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")

            cur.execute(buy_add_inventory, obj)
            cur.execute(get_user_stock, obj)
            current_amount = cur.fetchone()[0]
            if current_amount > 100:
                raise Exception("Cannot buy more than 100 products in total")

            conn.commit()
